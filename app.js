const express = require('express');
const { MongoClient } = require('mongodb');
const cors = require('cors');
const app = express();
const port = 8000;
const mongoURI = 'mongodb://localhost:27017/blackOffer';

app.use(express.json());
app.use(cors());
let client;

app.get('/api/data', async (req, res) => {
  try {
    client = new MongoClient(mongoURI, { useNewUrlParser: true, useUnifiedTopology: true });
    await client.connect();
    
    const db = client.db();
    const collection = db.collection('chart_data');
    
    const filters = {}; // Prepare an empty filter object
    
    // Apply filters based on query parameters
    if (req.query.endYear) {
      filters.end_year = req.query.endYear;
    }
    if (req.query.topic) {
      filters.topic = req.query.topic;
    }
    if (req.query.sector) {
      filters.sector = req.query.sector;
    }
    if (req.query.region) {
      filters.region = req.query.region;
    }
    if (req.query.pestle) {
      filters.pestle = req.query.pestle;
    }
    if (req.query.source) {
      filters.source = req.query.source;
    }
    if (req.query.swot) {
      filters.swot = req.query.swot;
    }
    if (req.query.country) {
      filters.country = req.query.country;
    }
    if (req.query.city) {
      filters.city = req.query.city;
    }
    
    // Add more filters based on your requirements
    
    const data = await collection.find(filters).toArray();
    
    res.json(data);
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Server error' });
  } finally {
    if (client) {
      client.close();
    }
  }
});

app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});
